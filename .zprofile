#!/bin/bash

export PATH="$HOME/.local/bin:$PATH"

export npm_config_prefix="$HOME/.local"

[ -d "$HOME/.cargo/bin" ] && export PATH="$HOME/.cargo/bin:$PATH"

[ -f "$HOME/.elan/env" ] && . "$HOME/.elan/env"
export PATH="$HOME/.elan/bin:$PATH"

export JAVA_HOME=/usr/lib/jvm/default/

export DOTNET_CLI_TELEMETRY_OPTOUT=1
[ -d "$HOME/.dotnet/tools" ] && export PATH="$HOME/.dotnet/tools:$PATH"


if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ]; then
	exec startx
elif [ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 2 ]; then
	exec sway
else . ~/.zshrc;
fi

