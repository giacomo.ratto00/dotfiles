local function isAnsible()
	local filepath = vim.fn.expand("%:.:h")
	local filename = vim.fn.expand("%:t:r")

	if vim.loop.fs_stat("ansible.cfg") == nil then
		return false
	end

	if filepath == "." then
		return true
	end

	-- match any path under roles except for roles/*/templates or roles/*/files
	local r = vim.regex( [[^roles/[^/]*/\%(\%(files\|templates\)\%($\|/\)\)\@!]] )

	return r:match_str(filepath) ~= nil
end

vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
	pattern = { "*.yml", "*.yaml" },
	callback = function (event)
		if isAnsible() then
			vim.opt_local.filetype = "ansible.yaml"
		end
	end
})
