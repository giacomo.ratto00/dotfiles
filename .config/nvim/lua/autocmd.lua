local autocmd = vim.api.nvim_create_autocmd

local function onWinEnter()
	if (vim.opt.number:get()) then
		vim.opt.relativenumber = true
	end
end

local group = vim.api.nvim_create_augroup('myautocmd', { clear = true })

-- autocmd("ColorScheme", { group = group, pattern = { "*" }, command = "highlight Normal ctermbg=NONE guibg=NONE" })

autocmd("WinEnter",
	{ group = group, pattern = { "*" }, callback = onWinEnter }
)

autocmd("WinLeave",
	{ group = group, pattern = { "*" }, command = "set norelativenumber" }
)

autocmd("FileType", {
	group = group,
	pattern = { "*" },
	callback = function()
		local ok, _ = pcall(vim.treesitter.get_parser, 0)
		if ok then
			vim.opt_local.foldmethod = "expr"
		end
	end
})

autocmd("VimEnter", {
	group = group,
	command = "clearjumps"
})

autocmd("FileType", {
	group = group,
	pattern = "sh",
	command = "setlocal iskeyword+=45",
})

autocmd('TextYankPost', {
	group = group,
	pattern = '*',
	callback = function()
		vim.highlight.on_yank({
			higroup = 'IncSearch',
			timeout = 50,
		})
	end,
})

local autorun = vim.api.nvim_create_augroup('autorun', { clear = true })

local output = {
	win = nil,
	buf = nil,
	job = nil
}

local latexmk = function(info)
	return {
		"bash",
		"-c",
		[[
				cd "$0" && latexmk -lualatex
			]],
		vim.fs.dirname(info.name)
	}
end

vim.g.autorun = {
	python = 'python "$0" < input',
	cpp = 'g++ -std=c++20 -g -Wfatal-errors -Wall -fsanitize=address "$0" && ./a.out < input',
	c = 'gcc -g -Wfatal-errors -Wall -fsanitize=address "$0" && ./a.out < input',
	-- 	tex = function(info)
	-- 		return {
	-- 			"bash",
	-- 			"-c",
	-- 			[[
	-- texfot --tee=/dev/null --ignore '^This is pdfTeX' \
	-- --ignore 'Output written' --ignore 'Output written' \
	-- pdflatex -file-line-error -halt-on-error -output-directory "$1" "$0" | \
	-- grep -v -e "`which texfot`:.*"; exit ${PIPESTATUS[0]} ]],
	-- 			info.name,
	-- 			vim.fs.dirname(info.name)
	-- 		}
	-- 	end,
	-- tex = latexmk,
	-- bib = latexmk,
	-- rust = 'rustc "$0" -o a.out && ./a.out < input'
	rust = [[cargo run --bin "`basename -s .rs \"$0\"`" ]],
	xdefaults = [[xrdb -merge "$0"]]
}

vim.api.nvim_create_user_command("AutorunOn", function()
	vim.api.nvim_create_autocmd("BufWritePost", {
		group = autorun,
		callback = function(o)

		end
	})
end, {})

local function make_table_from_cmd(how_to_run, bufnr)
	local info = vim.fn.getbufinfo(bufnr)[1]
	if type(how_to_run) == 'function' then
		return how_to_run(info)
	elseif type(how_to_run) == 'string' then
		return { "bash", "-c", how_to_run, info.name }
	end

	return how_to_run
end

autocmd('BufWritePost', {
	group = autorun,
	pattern = '*/codeforces/*,*.tex,*.bib,.Xdefaults',
	callback = function(o)
		local curbuf = vim.api.nvim_get_current_buf()
		local curwin = vim.api.nvim_get_current_win()

		local cmd = vim.b.autorun or
			vim.g.autorun[vim.api.nvim_buf_get_option(curbuf, 'filetype')]

		if cmd == nil then
			return
		end

		cmd = make_table_from_cmd(cmd, curbuf)

		if output.buf == nil or not vim.api.nvim_buf_is_valid(output.buf) then
			output.buf = vim.api.nvim_create_buf(true, true)
		end

		if output.win == nil or not vim.api.nvim_win_is_valid(output.win) then
			vim.cmd("bo vsplit")
			output.win = vim.api.nvim_get_current_win()
			vim.api.nvim_set_current_buf(output.buf)
			vim.opt_local.number = false
			vim.opt_local.relativenumber = false
			vim.opt_local.signcolumn = "no"
		end

		pcall(vim.fn.jobstop, jobid)

		vim.api.nvim_set_current_win(output.win)
		vim.api.nvim_set_current_buf(output.buf)

		vim.api.nvim_buf_set_option(output.buf, "modified", false)
		output.job = vim.fn.termopen(cmd)
		vim.api.nvim_set_current_buf(output.buf)
		-- vim.opt_local.winbar = string.format('Output: %s', vim.inspect(cmd))

		vim.api.nvim_set_current_win(curwin)
		if not output.win == curwin then
			vim.api.nvim_set_current_buf(curbuf)
		end
	end
})

vim.api.nvim_create_user_command("Bin", function()
	if vim.b.Bin then
		return
	end
	vim.cmd [[%!xxd -g 1]]
	vim.opt_local.modified = false
	vim.b.bin = true
	vim.b.bin_oldfiletype = vim.opt_local.filetype:get()
	vim.opt_local.filetype = "xxd" -- seriously?
	local cmd_id = vim.api.nvim_create_autocmd("BufWriteCmd", {
		buffer = vim.fn.bufnr("."),
		callback = function()
			vim.cmd [[silent w !xxd -r > %]]
		end,
	})
	vim.b.bin_cmd = cmd_id
end, { nargs = 0 })

vim.api.nvim_create_user_command("UnBin", function()
	if not vim.b.bin then
		return
	end
	vim.cmd [[%!xxd -r]]
	vim.opt_local.modified = false
	vim.b.Bin = false
	vim.opt_local.filetype = vim.b.bin_oldfiletype
	vim.api.nvim_del_autocmd(vim.b.bin_cmd)
end, { nargs = 0 })
