local common = require "common"
local bind = common.bind

vim.g.mapleader = " "

pcall(vim.keymap.del, 'n', "<leader>", { buffer = false })
pcall(vim.keymap.del, 'v', "<leader>", { buffer = false })

vim.keymap.set({ 'n', 'i' }, "<C-s>", function()
	vim.api.nvim_command("update")
end)

vim.keymap.set('i', "<C-c>", "<Esc>")

vim.keymap.set('n', "j", function()
	if (vim.v.count > 1) then
		return "m'" .. vim.v.count .. "j"
	end
	return "j"
end, { expr = true })

vim.keymap.set('n', "k", function()
	if (vim.v.count > 1) then
		return "m'" .. vim.v.count .. "k"
	end
	return "k"
end, { expr = true })

vim.keymap.set('n', "<leader>f", function()
	local ok, _ = pcall(require "telescope.builtin".git_files)
	if not ok then
		require "telescope.builtin".find_files()
	end
end)
vim.keymap.set('n', "<leader>F", require "telescope.builtin".find_files)
vim.keymap.set('n', "<leader>s", require "telescope.builtin".treesitter)
vim.keymap.set('n', "<leader>S", require "telescope.builtin".lsp_dynamic_workspace_symbols)

vim.keymap.set('n', "Q", "<nop>")
vim.keymap.set('n', "<S-CR>", "o<ESC>")
vim.keymap.set('n', "<leader>l", require "telescope.builtin".buffers)
vim.keymap.set('n', "<leader>d", function()
	vim.api.nvim_exec2([[bn
	bd #]], { output = false })
end)

-- -- I don't even know why i need this:
vim.keymap.set('n', "<C-i>", "<C-i>")
vim.keymap.set('n', "<tab>", "<cmd>bn<CR>")
vim.keymap.set('n', "<S-TAB>", "<cmd>bp<CR>")

vim.keymap.set('n', "<M-Tab>", "<cmd>tabnext<CR>")
vim.keymap.set('n', "<M-S-Tab>", "<cmd>tabprev<CR>")

vim.keymap.set('v', "<leader>/", "<ESC>/\\%V")
vim.keymap.set('i', "<C-BS>", "<ESC>ciw")

-- coconut oil
vim.keymap.set('n', "<leader>m", require("harpoon.mark").add_file)
vim.keymap.set('n', "<leader>h", require("harpoon.ui").toggle_quick_menu)

for i = 1, 9 do
	vim.keymap.set('n', "<leader>" .. tostring(i), bind(require("harpoon.ui").nav_file, i))
	vim.keymap.set('n', "<M-" .. tostring(i) .. ">", bind(require("harpoon.ui").nav_file, i))
end

vim.keymap.set('n', "<C-d>", "<C-d>zz")
vim.keymap.set('n', "<C-u>", "<C-u>zz")
vim.keymap.set('n', "n", "nzz")
vim.keymap.set('n', "N", "Nzz")


vim.keymap.set("v", "J", ":move '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":move '<-2<CR>gv=gv")

vim.keymap.set("n", "<leader>(", "<cmd>set operatorfunc=v:lua.require'common'.operators.parenthesis<CR>g@")
vim.keymap.set("n", "<leader>[", "<cmd>set operatorfunc=v:lua.require'common'.operators.square_bracket<CR>g@")
vim.keymap.set("n", "<leader>{", "<cmd>set operatorfunc=v:lua.require'common'.operators.curly_bracket<CR>g@")
vim.keymap.set("n", "<leader>\"", "<cmd>set operatorfunc=v:lua.require'common'.operators.double_quote<CR>g@")
vim.keymap.set("n", "<leader>'", "<cmd>set operatorfunc=v:lua.require'common'.operators.single_quote<CR>g@")

vim.keymap.set("v", "<leader>(", [[:<C-u>s/\%V\(\_.*\%V\_.\)/(\1)<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>)", [[:<C-u>s/\%V\(\_.*\%V\_.\)/(\1)<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>[", [[:<C-u>s/\%V\(\_.*\%V\_.\)/[\1]<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>]", [[:<C-u>s/\%V\(\_.*\%V\_.\)/[\1]<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>{", [[:<C-u>s/\%V\(\_.*\%V\_.\)/{\1}<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>}", [[:<C-u>s/\%V\(\_.*\%V\_.\)/{\1}<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>'", [[:<C-u>s/\%V\(\_.*\%V\_.\)/'\1'<CR>g`<]], { silent = true })
vim.keymap.set("v", "<leader>`", [[:<C-u>s/\%V\(\_.*\%V\_.\)/`\1`<CR>g`<]], { silent = true })
vim.keymap.set("v", [[<leader>"]], [[:<C-u>s/\%V\(\_.*\%V\_.\)/"\1"<CR>g`<]], { silent = true })

vim.keymap.set({ "o", "v" }, "il", ":<C-u>norm $v^<CR>", { silent = true })
vim.keymap.set({ "o", "v" }, "i/", ":<C-u>norm T/vt/<CR>", { silent = true })

local function get_indentation()
	local lnum = vim.fn.line(".")
	local last_ln = vim.fn.line("$")
	local indent = vim.fn.indent(lnum)

	if indent == 0 then
		return nil
	end

	local start_ln = lnum
	local end_ln = lnum

	while start_ln > 1 do
		local prev = vim.fn.prevnonblank(start_ln - 1)
		if vim.fn.indent(prev) < indent then
			break
		end
		start_ln = prev
	end

	while end_ln < last_ln do
		local next = vim.fn.nextnonblank(end_ln + 1)
		if vim.fn.indent(next) < indent then
			break
		end
		end_ln = next
	end

	return { start_ln, end_ln }
end

-- inside indentation
vim.keymap.set({ "o", "v" }, "ii", function()
	local i = get_indentation()
	if i == nil then
		return
	end
	vim.api.nvim_buf_set_mark(0, "[", i[1], 1, {})
	vim.api.nvim_buf_set_mark(0, "]", i[2], 1, {})
	vim.api.nvim_command "norm! `[Vo`]"
end)

-- around indentation
vim.keymap.set({ "o", "v" }, "ai", function()
	local i = get_indentation()
	if i == nil then
		return
	end
	-- same as inside but simply add a line at the beginning and end
	if i[1] > 1 then
		i[1] = vim.fn.prevnonblank(i[1] - 1)
	end

	if i[2] < vim.fn.line("$") then
		i[2] = vim.fn.nextnonblank(i[2] + 1)
	end
	vim.api.nvim_buf_set_mark(0, "[", i[1], 1, {})
	vim.api.nvim_buf_set_mark(0, "]", i[2], 1, {})
	vim.api.nvim_command "norm! `[Vo`]"
end)

vim.keymap.set("n", "<C-h>", "<C-w>h")
vim.keymap.set("n", "<C-j>", "<C-w>j")
vim.keymap.set("n", "<C-k>", "<C-w>k")
vim.keymap.set("n", "<C-l>", "<C-w>l")

-- lua scratch space
vim.keymap.set("n", "<leader><leader>s", function()
	local buf = vim.api.nvim_create_buf(true, true)
	vim.api.nvim_buf_set_option(buf, "filetype", "lua")
	-- instead of saving source it
	vim.api.nvim_buf_set_keymap(buf, "n", "<C-s>", ":so<CR>", { silent = false, noremap = true })
	vim.api.nvim_set_current_buf(buf)
	vim.cmd [[file .lua]]
end)

vim.cmd [[abbreviate !!! 🤡]]


-- TODO: add suppport for range
vim.keymap.set("n", "]q", "<Cmd>cnext<CR>")
vim.keymap.set("n", "[q", "<Cmd>cprevious<CR>")

vim.keymap.set("n", "<leader>t", "<cmd>Oil<cr>")
