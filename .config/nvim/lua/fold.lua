local m = {}

local fn = vim.fn
local v = vim.v

m.foldtext = function()
	local indent = fn.indent(v.foldstart) - vim.opt.tabstop:get()
	local pre = ""
	for _ = 0, indent do
		pre = pre .. "\t"
	end
	return pre .. "+" .. fn.getline(v.foldstart)
end

return m
