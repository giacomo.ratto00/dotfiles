local opt = vim.opt

opt.showmatch = true
opt.hlsearch = false
opt.incsearch = true

opt.tabstop = 4
opt.shiftwidth = 4
opt.autoindent = true
opt.smarttab = true
opt.expandtab = false

opt.number = true
opt.relativenumber = true
opt.signcolumn = 'yes'

opt.mouse = 'a'
opt.mousemodel = 'extend'

opt.swapfile = false
opt.hidden = true
opt.backup = false
opt.writebackup = false

opt.wrap = false
opt.linebreak = true
opt.breakindent = true

opt.title = true

opt.listchars = { tab = '  │', space = '·', --[[ eol = '↵', ]] extends = '»', precedes = '«' }
opt.list = true

opt.cursorline = false

opt.updatetime = 300

opt.scrolloff = 0
opt.sidescrolloff = 20

opt.termguicolors = true

opt.foldmethod = "indent"
opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.foldenable = false
opt.foldlevel = 10
opt.foldtext = "v:lua.require'fold'.foldtext()"

opt.wildmode = 'longest:full,full'
opt.completeopt = 'menuone,noinsert,noselect,preview'
opt.spelllang = 'en,it'
opt.spell = false

opt.undofile = true
opt.lazyredraw = true

opt.splitright = true
opt.splitbelow = true
opt.equalalways = false

opt.laststatus = 3
opt.jumpoptions = "stack,view"

vim.g.netrw_banner = 0
vim.g.netrw_liststyle = 0
vim.g.netrw_winsize = 80

vim.g.man_hardwrap = false

vim.g.tex_nospell = 0
vim.g.tex_comment_nospell = 1
vim.g.tex_verbspell = 0
