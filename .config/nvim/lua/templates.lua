local autocmd = vim.api.nvim_create_autocmd
local group = vim.api.nvim_create_augroup('templates', { clear = true })


autocmd("BufNewFile", { group = group, pattern = { "*/codeforces/*.cpp" }, command = "0r ~/.config/nvim/templates/codeforces.cpp" })
autocmd("BufNewFile", { group = group, pattern = { ".latexmkrc" }, command = "0r ~/.config/nvim/templates/latexmkrc" })
autocmd("BufNewFile", { group = group, pattern = { "Makefile" }, command = "0r ~/.config/nvim/templates/Makefile" })
