local M = {}

M.bind = function(f, arg)
	return function(...)
		return f(arg, ...)
	end
end

local function surround(with, type)
	local pos = vim.fn.getpos(".")
	local search_pattern = ({
		-- block??
		char = [[\%'\[\(\_.*\%'\]\_.\)]],
		line = [[\(.*\%'\[\_.*\%'\].*$\)]],
	})[type]
	local cmd = string.format([[s/%s/%s\1%s/]], search_pattern, with[1], with[2])
	vim.api.nvim_command(cmd)

	-- bring the cursor at the beginning of the range without updating jump list
	vim.fn.setpos(".", pos)
end

M.operators = {
	parenthesis = M.bind(surround, { '(', ')' }),
	square_bracket = M.bind(surround, { '[', ']' }),
	curly_bracket = M.bind(surround, { '{', '}' }),
	double_quote = M.bind(surround, { '"', '"' }),
	single_quote = M.bind(surround, { '\'', '\'' }),
	dollar = M.bind(surround, { '$', '$' }),
}

return M
