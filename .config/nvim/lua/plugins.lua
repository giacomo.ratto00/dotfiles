local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
	if fn.empty(fn.glob(install_path)) > 0 then
		fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
		vim.cmd [[packadd packer.nvim]]
		return true
	end
	return false
end

local packer_bootstrap = ensure_packer()

package.path = package.path .. ";" .. vim.fn.expand("$HOME") .. "/.luarocks/share/lua/5.1/?/init.lua;"
package.path = package.path .. ";" .. vim.fn.expand("$HOME") .. "/.luarocks/share/lua/5.1/?.lua;"
require 'packer'.startup(function(use)
	use 'wbthomason/packer.nvim'

	use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
	use 'nvim-treesitter/playground'
	use { 'nvim-treesitter/nvim-treesitter-textobjects', after = "nvim-treesitter", requires = "nvim-treesitter/nvim-treesitter", }

	-- lsp
	use "folke/neodev.nvim"
	use 'neovim/nvim-lspconfig'

	use 'numToStr/Comment.nvim'

	-- cmp
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-nvim-lua'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/nvim-cmp'
	use 'saadparwaiz1/cmp_luasnip'
	use 'L3MON4D3/LuaSnip'
	use 'f3fora/cmp-spell'
	use 'hrsh7th/cmp-nvim-lsp-signature-help'

	-- telescope
	use 'nvim-lua/plenary.nvim'
	use { 'nvim-telescope/telescope.nvim', tag = '0.1.x', requires = { { 'nvim-lua/plenary.nvim' } } }
	use { 'nvim-telescope/telescope-fzf-native.nvim',
		run =
		'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }

	-- dap
	use 'mfussenegger/nvim-dap'
	use { 'rcarriga/nvim-dap-ui', requires = { { 'nvim-neotest/nvim-nio' } } }
	use 'theHamsta/nvim-dap-virtual-text'
	use 'mfussenegger/nvim-dap-python'

	--colors
	use 'gruvbox-community/gruvbox'
	use { "catppuccin/nvim", as = "catppuccin" }

	use 'ThePrimeagen/harpoon'

	use 'lewis6991/gitsigns.nvim'
	use { 'NeogitOrg/neogit', requires = 'nvim-lua/plenary.nvim' }

	use 'windwp/nvim-autopairs'
	use 'williamboman/mason.nvim'

	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true }
	}

	vim.g.neo_tree_remove_legacy_commands = 1

	use({
		"stevearc/oil.nvim",
		config = function()
			require("oil").setup {
				default_file_explorer = true,
				buf_options = {
					buflisted = true,
					bufhidden = "",
				},
				cleanup_delay_ms = false,
			}
		end,
		requires = {
			"nvim-tree/nvim-web-devicons",
		}
	})

	-- use 'Julian/lean.nvim'
	-- use "hkupty/iron.nvim"
	-- use { "simrat39/rust-tools.nvim" }

	-- use '3rd/image.nvim'
	-- use 'mhinz/vim-rfc'
	-- use 'HiPhish/info.vim'

	-- use {
	-- 	'HakonHarnes/img-clip.nvim',
	-- 	config = function()
	-- 		require("img-clip").setup()
	-- 	end
	-- }

	-- use { 't-troebst/perfanno.nvim', config = function()
	-- 	require("perfanno").setup()
	-- end }

	if packer_bootstrap then
		require('packer').sync()
	end
end)

-- vim.opt.runtimepath:prepend(vim.fn.expand("$HOME") .. "/Documents/code/info.vim/")
-- vim.opt.runtimepath:append(vim.fn.expand("$HOME") .. "/Documents/code/jupytertest/")
-- vim.opt.runtimepath:append(vim.fn.expand("$HOME") .. "/Documents/code/rofi-ui-select.nvim/")

-- require("rofi-select").setup()
