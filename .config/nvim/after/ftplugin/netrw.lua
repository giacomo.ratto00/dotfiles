-- it deletes the keymap but for some reason it is called again and raises an error
pcall(vim.api.nvim_buf_del_keymap, 0, "n", "<C-l>")
pcall(vim.api.nvim_buf_del_keymap, 0, "n", "<F1>")
