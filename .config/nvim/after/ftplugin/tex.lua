vim.opt_local.spell = true

vim.keymap.set("n",
	"<leader>$",
	"<cmd>set operatorfunc=v:lua.require'common'.operators.dollar<CR>g@",
	{ buffer = true }
)
