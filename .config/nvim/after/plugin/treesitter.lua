local disable = function(lang, buf)
	local max_filesize = 100 * 1024
	local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
	return ok and stats and stats.size > max_filesize
end

require 'nvim-treesitter.configs'.setup {
	ensure_installed = {
		"c",
		"cpp",
		"javascript",
		"rust",
		"lua",
		"typescript",
		"python",
		-- "latex"
	},
	sync_install = false,
	auto_install = false,
	-- ignore_install = {},
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
		-- disable = {
		-- 	"proto",
		-- 	"help",
		-- },
		disable = disable,
	},
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = "<leader>v",
			node_incremental = "	",
			-- scope_incremental = "grc",
			node_decremental = "<S-Tab>",
		},
	},
	textobjects = {
		select = {
			enable = true,
			lookahead = true,
			keymaps = {
				-- Built-in captures.
				["af"] = "@function.outer",
				["if"] = "@function.inner",
				["aB"] = "@block.outer",
				["iB"] = "@block.inner",
				["ac"] = "@comment.outer",
				-- ["aF"] = "@frame.outer",
				-- ["iF"] = "@frame.inner",
			},
			disable = disable
		},
		move = {
			enable = true,
			set_jumps = true,
			goto_next_start = {
				["]f"] = "@function.outer",
			},
			goto_previous_start = {
				["[f"] = "@function.outer",
			}
		}
	}
}

