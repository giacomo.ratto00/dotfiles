local cmp = require 'cmp'
if (cmp == nil) then return end
cmp.setup {
	enabled = function()
		return vim.api.nvim_buf_get_option(0, "buftype") ~= "prompt"
	end,
	preselect = cmp.PreselectMode.Item,
	--
	sources = {
		{ name = 'nvim_lsp_signature_help' },
		{ name = "nvim_lsp" },
		{ name = "nvim_lua" },
		{ name = 'luasnip' },
		-- { name = "spell" },
		-- { name = "path", enable_in_context = function()
		-- 	return vim.opt.filetype:get() ~= "TelescopePrompt"
		-- end },
		-- { name = "buffer", keyword_length = 5 },
	},
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		['<C-u>'] = cmp.mapping.scroll_docs(-4),
		['<C-d>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }),
		['<TAB>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				return cmp.complete_common_string()
			end
			fallback()
		end),
		-- ['<S-TAB>'] = cmp.mapping.select_prev_item()
	}),
	view = {
		-- horrors beyond my comprehension:
		-- https://github.com/hrsh7th/nvim-cmp/issues/1400
		-- entries = "native"
	},
	expandable_indicator = true,
	experimental = {
		ghost_text = true,
	},
	formatting = {
		-- fields = { 'abbr', 'kind', 'menu' },
		fields = { 'abbr', 'kind', },
		format = function(_, vim_item)
			-- if vim_item.menu ~= nil then
			-- 	vim_item.menu = string.sub(vim_item.menu, 1, 24)
			-- end
			vim_item.menu = nil
			return vim_item
		end,
	},
}

-- vim.keymap.set("i", "<C-x><C-f>", function()
-- 	cmp.complete {
-- 		config = {
-- 			sources = {
-- 				{ name = "path" }
-- 			}
-- 		}
-- 	}
-- end)
--
