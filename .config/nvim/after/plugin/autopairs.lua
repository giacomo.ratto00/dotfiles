local npairs = require 'nvim-autopairs'
local Rule = require 'nvim-autopairs.rule'

npairs.setup()

local tex_ft = { "tex", "latex" }

npairs.add_rules {
	Rule('$', '$', tex_ft)
}

npairs.get_rule("'")[1].not_filetypes = tex_ft
npairs.get_rule('"')[1].not_filetypes = tex_ft
npairs.get_rule('`').not_filetypes = tex_ft
