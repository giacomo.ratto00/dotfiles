local telescope = require 'telescope'

telescope.setup {
	extensions = {
		fzf = {
			fuzzy = true,          -- false will only do exact matching
			override_generic_sorter = true, -- override the generic sorter
			override_file_sorter = true, -- override the file sorter
			case_mode = "smart_case", -- or "ignore_case" or "respect_case"
		}
	}
}

telescope.load_extension('fzf')

vim.keymap.set("n", '<leader>f', require 'telescope.builtin'.find_files)
vim.keymap.set("n", '<F1>', require 'telescope.builtin'.help_tags)
vim.keymap.set("n", '<leader>g', function()
	require 'telescope.builtin'.live_grep({
		glob_pattern = { "!*.obj", "!*.svg" }
	})
end)
vim.keymap.set("n", "<leader>*", require "telescope.builtin".grep_string)
vim.keymap.set("n", '<leader><F1>', function()
	require 'telescope.builtin'.man_pages({ sections = { 'ALL' } })
end)
