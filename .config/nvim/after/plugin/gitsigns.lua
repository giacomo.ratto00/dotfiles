local ok, gitsigns = pcall(require, 'gitsigns')
if not ok then
	return
end

gitsigns.setup()

local gistsign_group = vim.api.nvim_create_augroup("gitsign_attach", { clear = true })

vim.api.nvim_create_autocmd({ "BufReadPost" }, {
	pattern = vim.fn.expand("$HOME") .. "/.config/*",
	group = gistsign_group,
	callback = function(event)
		local opt = {
			file = string.sub(vim.fn.expand("%:~"), 3),
			toplevel = vim.fn.expand("$HOME"),
			gitdir = vim.fn.expand("$HOME") .. "/.dotfiles/"
		}
		gitsigns.attach(vim.fn.bufnr(), opt)
	end
})
