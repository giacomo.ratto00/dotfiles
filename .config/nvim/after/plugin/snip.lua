local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local m = extras.m
local l = extras.l
local rep = extras.rep
local postfix = extras.postfix
local types = require 'luasnip.util.types'

local snippet_collection = require("luasnip.session.snippet_collection")

ls.config.set_config {
	history = true,

	updateevents = "TextChanged,TextChangedI",
	delete_check_events = "TextChanged,TextChangedI",

	enable_autosnippets = true,

	ext_opts = {
		[types.choiceNode] = {
			active = {
				virt_text = { { "← choice", "Error" } },
			},
		},
		[types.insertNode] = {
			active = {
				virt_text = { { "← insert", "Error" } }
			}
		},
		-- [types.snippet] = {
		-- 	active = {
		-- 		virt_text = { { "snippet", "NonTest" } }
		-- 	}
		-- }
	},
}

vim.keymap.set({ 'i', 's' }, '<C-k>', ls.expand_or_jump, { silent = true })

vim.keymap.set({ 'i', 's' }, '<C-j>', function()
	ls.jumpable(-1)
end, { silent = true })

vim.keymap.set({ 'i', 's' }, '<C-l>', function()
	if ls.choice_active() then
		ls.change_choice(1)
	end
end, { silent = true })

vim.keymap.set({ 'i', 's' }, '<C-h>', function()
	if ls.choice_active() then
		ls.change_choice(-1)
	end
end, { silent = true })

-- ls.add_snippets("all", s({ trig = "tii", dscr = "Expands 'tii' into LaTeX's textit{} command." },
-- 	fmta("\\textit{<>}",
-- 		{
-- 			d(1, get_visual),
-- 		}
-- 	)
-- ))

-- ls.add_snippets("all", s(
-- 	{ name = "begin", trig = [[\begin]], snippetType = "autosnippet", hidden = true },
-- 	fmt(
-- 		[[
-- \begin{<>}
-- 	<>
-- \end{<>}
-- ]], { i(1), i(0), rep(1) }, { delimiters = "<>" })))

ls.add_snippets("tex", {
	s({ name = "begin", trig = [[\begin]], snippetType = "autosnippet", hidden = true }, {
		t("\\begin{"),
		i(1),
		t({ "}", "\t" }),
		i(0),
		t({ "", "\\end{" }),
		rep(1),
		t("}")
	}
	)
})

ls.add_snippets("tex", {
	s({ name = "texttt", trig = [[\tt]], snippetType = "autosnippet", hidden = true }, {
		t("\\texttt{"),
		i(0),
		t("}"),
	})
})


-- nnoremap('<leader><leader>s', ':source ~/.config/nvim/lua/plugins/snip.lua<CR>')

--ls.snippets = {
--all = {
--ls.parser.parse_snippet('test', 'epic')
--}
--}

-- ls.add_snippets('all', {
-- 	s("fn", {
-- 		-- Simple static text.
-- 		t("//Parameters: "),
-- 		-- function, first parameter is the function, second the Placeholders
-- 		-- whose text it gets as input.
-- 		--f(copy, 2),
-- 		t({ "", "function " }),
-- 		-- Placeholder/Insert.
-- 		i(1),
-- 		t("("),
-- 		-- Placeholder with initial text.
-- 		i(2, "int foo"),
-- 		-- Linebreak
-- 		t({ ") {", "\t" }),
-- 		-- Last Placeholder, exit Point of the snippet.
-- 		i(0),
-- 		t({ "", "}" }),
-- 	}),
-- })

-- ls.add_snippets('lua', {
-- 	s('req', fmt('local {} = require \'{}\'', { i(0), rep(0) }))
-- })
--
-- ls.add_snippets('d', {
-- 	s("fn", fmt("{} {}({}) {{\n\t{}\n}}", { i(1), i(2), i(3), i(0) }))
-- })

-- local function cond()
-- 	return not ls.jumpable()
-- end
--
-- ls.add_snippets('all', { s({ trig = '(', hidden = true }, { t '(', i(1), t ')', i(0) }, { condition = cond }) })
--
-- ls.add_snippets('all', { s({ trig = '{', hidden = true }, { t '{', i(1), t '}', i(0) }, { condition = cond }) })

--ls.add_snippets('d', {
--s('if',
--fmt({'if ({})','{}'},
--{
--i(1),
--c(2, {
--t 'epic',
----sn(nil, {
------t 'ns'
----fmt('{{\n{}\n}}', i(1)),
----}),
----i(2)
--i(2)
--}),
----i(3)
--}
--)
--)
--})
