require 'mason'.setup {
	-- install_root_dir = vim.fn.stdpath "data" .. '/' .. "mason",

	-- Where Mason should put its bin location in your PATH. Can be one of:
	-- - "prepend" (default, Mason's bin location is put first in PATH)
	-- - "append" (Mason's bin location is put at the end of PATH)
	-- - "skip" (doesn't modify PATH)
	---@type '"prepend"' | '"append"' | '"skip"'
	-- PATH = "skip",
	-- PATH = "append",
}
