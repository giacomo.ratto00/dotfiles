-- vim.g.gruvbox_contrast_dark = 'hard'
-- vim.opt.background = "dark"
-- vim.cmd('colorscheme gruvbox')

-- vim.api.nvim_set_hl(0, "Visual", { bold = true, bg = "#615853", reverse = false })
-- vim.cmd [[hi InactiveWin guibg=#151717]]
-- vim.cmd [[hi markdownError guifg=None guibg=None]]


-- require('catppuccin').setup {
-- 	compile_path = vim.fn.stdpath "cache" .. "/catppuccin"
-- }

require("catppuccin").setup {
	color_overrides = {
		latte = {
			base = "#ffffff",
			blue = "#1c60e8",
			lavender = "#6173d9",
			text = "#000000",
		},
		frappe = {},
		macchiato = {},
		mocha = {
			base = "#000000",
			text = "#ffffff"
		},
	}
}
vim.cmd [[colorscheme catppuccin-mocha]]

-- default latte
-- {
--   base = "#eff1f6",
--   blue = "#1e66f6",
--   crust = "#dce0e9",
--   flamingo = "#dd7879",
--   green = "#40a02c",
--   lavender = "#7287fe",
--   mantle = "#e6e9f0",
--   maroon = "#e64554",
--   mauve = "#8839f0",
--   overlay0 = "#9ca0b1",
--   overlay1 = "#8c8fa2",
--   overlay2 = "#7c7f94",
--   peach = "#fe640c",
--   pink = "#ea76cc",
--   red = "#d20f3a",
--   rosewater = "#dc8a79",
--   sapphire = "#209fb6",
--   sky = "#04a5e6",
--   subtext0 = "#6c6f86",
--   subtext1 = "#5c5f78",
--   surface0 = "#ccd0db",
--   surface1 = "#bcc0cd",
--   surface2 = "#acb0bf",
--   teal = "#17929a",
--   text = "#4c4f6a",
--   yellow = "#df8e1e"
-- }

-- default mocha
-- {
--   base = "#1e1e2f",
--   blue = "#89b4fb",
--   crust = "#11111c",
--   flamingo = "#f2cdce",
--   green = "#a6e3a2",
--   lavender = "#b4beff",
--   mantle = "#181826",
--   maroon = "#eba0ad",
--   mauve = "#cba6f8",
--   overlay0 = "#6c7087",
--   overlay1 = "#7f849d",
--   overlay2 = "#9399b3",
--   peach = "#fab388",
--   pink = "#f5c2e8",
--   red = "#f38ba9",
--   rosewater = "#f5e0dd",
--   sapphire = "#74c7ed",
--   sky = "#89dcec",
--   subtext0 = "#a6adc9",
--   subtext1 = "#bac2df",
--   surface0 = "#313245",
--   surface1 = "#45475b",
--   surface2 = "#585b71",
--   teal = "#94e2d6",
--   text = "#cdd6f5",
--   yellow = "#f9e2b0"
-- }
-- -- INSERT --
