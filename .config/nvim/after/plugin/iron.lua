local ok, iron = pcall(require, "iron.core")

if not ok then
	return
end

iron.setup {
	config = {
		-- Whether a repl should be discarded or not
		scratch_repl = false,
		-- Your repl definitions come here
		repl_definition = {
			sh = {
				-- Can be a table or a function that
				-- returns a table (see below)
				command = { "zsh" }
			}
		},
		-- How the repl window will be displayed
		-- See below for more information
		repl_open_cmd = require('iron.view').split.vertical.topleft(0.4),
	},
	-- Iron doesn't set keymaps by default anymore.
	-- You can set them here or manually add keymaps to the functions in iron.core
	keymaps = {
		send_motion = "<space>x",
		visual_send = "<space>x",
		send_file = "<space>xf",
		send_line = "<space>xx",
		-- send_mark = "<space>xm",
		interrupt = "<space><space>xi",
		exit = "<space><space>xq",
		clear = "<space><space>xc",
	},
	-- If the highlight is on, you can change how it looks
	-- For the available options, check nvim_set_hl
	highlight = {
		italic = false
	},
	ignore_blank_lines = true, -- ignore blank lines when sending visual select lines
}

-- iron also has a list of commands, see :h iron-commands for all available commands
-- vim.keymap.set('n', '<space>rs', '<cmd>IronRepl<cr>')
-- vim.keymap.set('n', '<space>rr', '<cmd>IronRestart<cr>')
-- vim.keymap.set('n', '<space>rf', '<cmd>IronFocus<cr>')
-- vim.keymap.set('n', '<space>rh', '<cmd>IronHide<cr>')
