local dap = require 'dap'
local dapui = require 'dapui'

require("nvim-dap-virtual-text").setup {}

vim.fn.sign_define("DapBreakpoint", { text = "🔴", texthl = "", linehl = "", numhl = "" })
vim.fn.sign_define("DapBreakpointCondition", { text = "🔵", texthl = "", linehl = "", numhl = "" })

dap.adapters.node2 = {
	type = 'executable',
	command = 'node',
	-- args = { 'node-debug2-adapter' },
}

dap.configurations.javascript = {
	{
		name = 'Launch',
		type = 'node2',
		request = 'launch',
		program = '${file}',
		cwd = vim.fn.getcwd(),
		sourceMaps = true,
		protocol = 'inspector',
		console = 'integratedTerminal',
	},
	-- {
	-- 	-- For this to work you need to make sure the node process is started with the `--inspect` flag.
	-- 	name = 'Attach to process',
	-- 	type = 'node2',
	-- 	request = 'attach',
	-- 	processId = require 'dap.utils'.pick_process,
	-- },
}

dap.configurations.typescript = {
	{
		name = 'Launch',
		type = 'node2',
		request = 'launch',
		program = '${file}',
		cwd = vim.fn.getcwd(),
		sourceMaps = true,
		protocol = 'inspector',
		console = 'integratedTerminal',
	},
	-- {
	-- 	-- For this to work you need to make sure the node process is started with the `--inspect` flag.
	-- 	name = 'Attach to process',
	-- 	type = 'node2',
	-- 	request = 'attach',
	-- 	processId = require 'dap.utils'.pick_process,
	-- },
}

dap.adapters.cppdbg = {
	id = 'cppdbg',
	type = 'executable',
	command = 'OpenDebugAD7',
}

local last_bin = nil
dap.configurations.cpp = {
	{
		name = 'Launch',
		type = 'cppdbg',
		request = 'launch',
		program = function()
			if last_bin == nil then
				last_bin = vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
			else
				last_bin = vim.fn.input('Path to executable: ', last_bin, 'file')
			end
			return last_bin
		end,
		cwd = '${workspaceFolder}',
		stopOnEntry = false,
		args = {},
		setupCommands = {
			{
				text = '-enable-pretty-printing',
				description = 'enable pretty printing',
				ignoreFailures = false
			},
		},
		-- 💀
		-- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
		--
		--    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
		--
		-- Otherwise you might get the following error:
		--
		--    Error on launch: Failed to attach to the target process
		--
		-- But you should be aware of the implications:
		-- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
		runInTerminal = false,
	},
}

dapui.setup()

dap.configurations.c = dap.configurations.cpp

dap.adapters.python = {
	id = "debugpy",
	type = 'executable',
	command = 'debugpy-adapter',
}

dap.configurations.python = {
	{
		type = "python",
		request = "launch",
		name = "launch current file",

		-- debugpy
		program = "${file}",
		pythonPath = '/usr/bin/python3'
	}, {
	type = "python",
	request = "launch",
	name = "pick file to launch",

	-- debugpy
	program = function()
		return vim.fn.input('Path to script: ', vim.fn.getcwd() .. '/', 'file')
	end,
	pythonPath = '/usr/bin/python3'
}
}

dap.configurations.rust = dap.configurations.cpp
dap.configurations.d = dap.configurations.cpp

local tabid = -1
dap.listeners.after.event_initialized["dapui_config"] = function()
	if not vim.api.nvim_tabpage_is_valid(tabid) then
		vim.api.nvim_command("tab split");
		tabid = vim.api.nvim_tabpage_get_number(0);
		vim.api.nvim_tabpage_set_var(tabid, "tabline", "Debugger")
	end
	vim.api.nvim_set_current_tabpage(tabid)
	dapui.open()
end

-- dap.listeners.before.event_terminated["dapui_config"] = function()
-- 	dapui.close()
-- end
-- dap.listeners.before.event_exited["dapui_config"] = function()
-- 	dapui.close()
-- end

vim.keymap.set("n", '<F3>', dap.step_into)
vim.keymap.set("n", '<F4>', dap.step_over)
vim.keymap.set("n", '<F5>', dap.continue)
vim.keymap.set("n", '<S-F5>', dap.restart)
vim.keymap.set("n", '<F6>', dap.step_out)
vim.keymap.set("n", '<leader>b', dap.toggle_breakpoint)
vim.keymap.set("n", '<leader>B', function()
	dap.set_breakpoint(vim.fn.input('Breakpoint condition: '))
end)
vim.keymap.set("n", "<space>?", function()
	require("dapui").eval(nil, { enter = true })
end)
