local comment = require 'Comment'

comment.setup {
	padding = true,
	---Whether the cursor should stay at its position
	sticky = true,
	---Lines to be ignored while (un)comment
	ignore = nil,
	---LHS of toggle mappings in NORMAL mode
	toggler = {
		---Line-comment toggle keymap
		line = '<leader>cc',
		---Block-comment toggle keymap
		-- block = '<leader>cb',
	},
	opleader = {
		---Line-comment keymap
		line = '<leader>c',
		---Block-comment keymap
		-- block = '<leader>b',
	}
}
