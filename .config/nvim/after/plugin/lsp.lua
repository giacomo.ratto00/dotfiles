vim.diagnostic.config {
	virtual_text = {
		severity = { min = vim.diagnostic.severity.ERROR },
	},
	underline = {
		severity = { min = vim.diagnostic.severity.ERROR }
	},
	signs = {
		severity = { min = vim.diagnostic.severity.INFO }
	}
}

local toggle_diagnostic = vim.api.nvim_create_augroup("toggle_diagnostic", { clear = true })


vim.api.nvim_create_autocmd("InsertEnter", {
	group = toggle_diagnostic,
	callback = function()
		vim.diagnostic.config {
			virtual_text = false,
			underline = false,
			signs = true
		}
	end
})

vim.api.nvim_create_autocmd("InsertLeave", {
	group = toggle_diagnostic,
	callback = function()
		vim.diagnostic.config {
			virtual_text = true,
			underline = true,
			signs = true
		}
	end
})

vim.lsp.handlers["textDocument/publishDiagnostics"] = function (...)
	vim.lsp.diagnostic.on_publish_diagnostics(...)
	local title = vim.fn.getqflist({title=1}).title
	if (title == "Diagnostics") then
		vim.diagnostic.setqflist({ open = false })
	end
end


local on_attach = function(client, bufnr)
	-- client.server_capabilities.semanticTokensProvider = nil
	-- client.server_capabilities.completion.snippetSupport = true
	--
	-- Mappings.
	local bufopts = { silent = true, buffer = bufnr }
	vim.keymap.set("n", 'gD', vim.lsp.buf.declaration, bufopts)
	vim.keymap.set("n", '<C-w>gD', [[:vsplit +norm\ gD<CR>]], bufopts)
	vim.keymap.set("n", 'gd', vim.lsp.buf.definition, bufopts)
	vim.keymap.set("n", '<C-w>gd', [[:vsplit +norm\ gd<CR>]], bufopts)
	vim.keymap.set("n", 'gt', vim.lsp.buf.type_definition, bufopts)
	vim.keymap.set("n", '<C-w>gt', [[:vsplit +norm\ gt<CR>]], bufopts)
	vim.keymap.set("n", 'K', vim.lsp.buf.hover, bufopts)
	vim.keymap.set("n", 'gi', vim.lsp.buf.implementation, bufopts)
	vim.keymap.set("n", '<C-w>gi', [[:vsplit +norm\ gi<CR>]], bufopts)
	vim.keymap.set("n", '<leader>wa', vim.lsp.buf.add_workspace_folder, bufopts)
	vim.keymap.set("n", '<leader>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
	vim.keymap.set("n", '<leader>p', function()
		local lwindow_id = vim.fn.getqflist({winid = 1}).winid
		if (lwindow_id == 0) then
			vim.diagnostic.setqflist()
		else
			vim.api.nvim_win_close(lwindow_id, false)
		end
	end, bufopts)
	-- vim.keymap.set("n", '<leader>p', vim.diagnostic.open_float, bufopts)
	vim.keymap.set("n", "]d", vim.diagnostic.goto_next, bufopts)
	vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, bufopts)
	vim.keymap.set("n", '<leader>wl', function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, bufopts)

	-- yes i used to use VS***e
	vim.keymap.set("n", '<F2>', vim.lsp.buf.rename, bufopts)
	-- vim.keymap.set("n", '<leader>ca', vim.lsp.buf.code_action, bufopts)
	vim.keymap.set("n", 'gr', vim.lsp.buf.references, bufopts)
	vim.keymap.set("n", '<leader>i', function()
		vim.lsp.buf.format { async = true }
	end, bufopts)

	vim.keymap.set({ "v", "n" }, '<M-CR>', vim.lsp.buf.code_action, bufopts)

	-- vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
	vim.api.nvim_set_option_value('omnifunc', 'v:lua.vim.lsp.omnifunc', {
		buf = bufnr
	})
	if vim.opt_local.filetype:get() ~= "tex" then
		-- vim.api.nvim_buf_set_option(bufnr, 'formatexpr', 'v:lua.vim.lsp.formatexpr(#{timeout_ms:250})')
		vim.api.nvim_set_option_value('formatexpr', 'v:lua.vim.lsp.formatexpr(#{timeout_ms:250})', {
			buf = bufnr
		})

		if vim.version() >= vim.version.parse("0.10.0-dev") then
			vim.lsp.inlay_hint.enable(true)
		end
	end
	-- if client.server_capabilities.inlayHintProvider then
	-- 	vim.lsp.buf.inlay_hint(bufnr, true)
	-- end
end

require 'lspconfig.util'.default_config.on_attach = on_attach
require 'lspconfig.util'.default_config.capabilities = vim.tbl_extend("force",
	vim.lsp.protocol.make_client_capabilities(),
	require('cmp_nvim_lsp').default_capabilities())

require 'lspconfig'.clangd.setup {}

-- require 'lspconfig'.ccls.setup {}

require 'lspconfig'.texlab.setup {}

-- require 'lspconfig'.yamlls.setup {}

-- require 'lspconfig'.dockerls.setup {}
require 'lspconfig'.docker_compose_language_service.setup {
	filetypes = { "docker-compose.yaml" }
}

require 'lspconfig'.kotlin_language_server.setup {}

require("neodev").setup({})

require 'lspconfig'.lua_ls.setup {
	settings = {
		Lua = {
			runtime = {
				version = "LuaJIT",
				path = vim.split(package.path, ";"),
			},
			diagnostics = {},
			workspace = {
				library = vim.opt.runtimepath:get(),
			},
			telemetry = { enable = false }
		},
	}
}

require 'lspconfig'.ts_ls.setup {}

require 'lspconfig'.zls.setup {}

require 'lspconfig'.rust_analyzer.setup {
	-- cmd = { "rustup", "run", "nightly", "rust-analyzer" },
	settings = {
		['rust-analyzer'] = {
			checkOnSave = {
				allFeatures = true,
				overrideCommand = {
					'cargo', 'clippy', '--workspace', '--message-format=json',
					'--all-targets', '--all-features', '--', '-A', 'clippy::missing_safety_doc'
				}
			},
			completion = {
				autoimport = {
					enable = false
				}
			}
		}
	}
}

require 'lspconfig'.serve_d.setup {}

require 'lspconfig'.jdtls.setup {
	cmd = { "jdtls", "-configuration",
		"/home/giacomo/.cache/jdtls/config", "-data",
		"/home/giacomo/.cache/jdtls/workspace" },
}

require 'lspconfig'.pylsp.setup {
	settings = {
		pylsp = {
			plugins = {
				autopep8 = {
					enabled = false
				}
			}
		}
	}
}

require 'lspconfig'.ansiblels.setup {
	filetypes = { "ansible.yaml" }
}

require 'lspconfig'.gopls.setup {}

require 'lspconfig'.cmake.setup {}

local ok, lean = pcall(require, "lean")
if ok then
	lean.setup {
		abbreviations = { builtin = true },
		lsp = { on_attach = on_attach },
		-- lsp3 = { on_attach = on_attach },
		mappings = false,
	}
end

require 'lspconfig'.hls.setup {
	filetypes = { 'haskell', 'lhaskell', 'cabal' },
}

require 'lspconfig'.csharp_ls.setup {}
require 'lspconfig'.phpactor.setup {}
-- require 'lspconfig'.perlnavigator.setup {}
--

-- local rust_tools = require "rust-tools"
--
-- rust_tools.setup {
-- 	server = {
-- 		on_attach = function(client, bufnr)
-- 			on_attach(client, bufnr)
-- 			rust_tools.inlay_hints.enable()
-- 		end
-- 	}
-- }
