vim.loader.enable()
require 'plugins'
require 'opt'
require 'keymap'
require 'autocmd'
require 'templates'

