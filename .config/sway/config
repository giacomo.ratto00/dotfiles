include input_output
include startup

set $rosewater #f5e0dc
set $flamingo  #f2cdcd
set $pink      #f5c2e7
set $mauve     #cba6f7
set $red       #f38ba8
set $maroon    #eba0ac
set $peach     #fab387
set $green     #a6e3a1
set $teal      #94e2d5
set $sky       #89dceb
set $sapphire  #74c7ec
set $blue      #89b4fa
set $lavender  #b4befe
# set $text      #cdd6f4
set $text      #ffffff
set $subtext1  #bac2de
set $subtext0  #a6adc8
set $overlay2  #9399b2
set $overlay1  #7f849c
set $overlay0  #6c7086
set $surface2  #585b70
set $surface1  #45475a
set $surface0  #313244
set $base      #1e1e2e
set $mantle    #181825
set $crust     #11111b


# i3 config file (v4)
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
# font pango:monospace 8
font pango:Hack Nerd Font, 8

# Use pactl to adjust volume in PulseAudio.
set $mysink "@DEFAULT_SINK@"
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume $mysink +1% 
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume $mysink -1%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute $mysink toggle && $refresh_i3status
bindsym $mod+Prior exec --no-startup-id pactl set-sink-volume $mysink +1% 
bindsym $mod+Next exec --no-startup-id pactl set-sink-volume $mysink -1%

set $mysource "@DEFAULT_SOURCE@"
bindsym $mod+XF86AudioMute exec --no-startup-id pactl set-source-mute $mysource toggle
bindsym $mod+End exec --no-startup-id pactl set-source-mute $mysource toggle && $refresh_i3status

# bindsym Menu exec --no-startup-id pactl set-source-mute $mysource 0
# bindsym --relase Menu exec --no-startup-id pactl set-source-mute $mysource 1

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec $HOME/.config/i3/i3_shell.sh

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
#bindsym $mod+d exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop
bindsym $mod+d exec --no-startup-id j4-dmenu-desktop

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+c split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+z focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
# These bindings trigger as soon as you enter the resize mode

# Pressing left will shrink the window’s width.
# Pressing right will grow the window’s width.
# Pressing up will shrink the window’s height.
# Pressing down will grow the window’s height.
	bindsym h resize shrink width 10 px or 10 ppt
		bindsym j resize grow height 10 px or 10 ppt
		bindsym k resize shrink height 10 px or 10 ppt
		bindsym l resize grow width 10 px or 10 ppt

		bindsym Shift+h resize shrink width 1 px or 1 ppt
		bindsym Shift+j resize grow height 1 px or 1 ppt
		bindsym Shift+k resize shrink height 1 px or 1 ppt
		bindsym Shift+l resize grow width 1 px or 1 ppt

# same bindings, but for the arrow keys
		bindsym Left resize shrink width 10 px or 10 ppt
		bindsym Down resize grow height 10 px or 10 ppt
		bindsym Up resize shrink height 10 px or 10 ppt
		bindsym Right resize grow width 10 px or 10 ppt

# back to normal: Enter or Escape or $mod+r
		bindsym Return mode "default"
		bindsym Escape mode "default"
		bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# target                 title     bg    text   indicator  border
# client.focused           $pink     $base $text  $rosewater $pink
# client.focused_inactive  $mauve    $base $text  $rosewater $mauve
# client.unfocused         $mauve    $base $text  $rosewater $mauve
# client.urgent            $peach    $base $peach $overlay0  $peach
# client.placeholder       $overlay0 $base $text  $overlay0  $overlay0
# client.background        $base

client.focused           $base     $blue $base  $rosewater $blue
client.focused_inactive  $overlay2 $base $text  $rosewater $surface2
client.unfocused         $surface0 $base $text  $rosewater $surface0
client.urgent            $red      $base $red   $overlay0  $red
client.placeholder       $overlay0 $base $text  $overlay0  $overlay0
client.background        $base

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)

bar {
	colors {
		background $base
		statusline $text
		separator  $text
		focused_workspace  $base $blue $base
		active_workspace   $base $overlay2 $base
		inactive_workspace $base $base $text
		urgent_workspace   $base $red $crust
		binding_mode       $base $red $crust
	}
	# output primary
	position bottom
	status_command i3status
	mode dock
}

# bar {
# 	id 2
# 	colors {
# 		background $base
# 		statusline $text
# 		separator  $text
# 		focused_workspace  $base $blue $base
# 		active_workspace   $base $overlay2 $base
# 		inactive_workspace $base $base $text
# 		urgent_workspace   $base $red $crust
# 		binding_mode       $base $red $crust
# 	}
# 	output nonprimary
# }

# screenshot
bindsym $mod+Shift+s exec grim -g "$(slurp -d)" - | wl-copy
bindsym Print exec grim - | wl-copy
# bindsym $mod+Print exec grim -g "$(swaymsg -t get_tree | jq -r '.. | select(.visible? and .focused?) | "\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height)" ')" - | wl-copy
# bindsym $mod+Print exec notify-send "$(swaymsg -t get_tree | jq -r '.. | select(.visible? and .focused?) | "\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height)" ')"
# bindsym $mod+Print exec notify-send "$(swaymsg -t get_tree | jq -r '.. | select(.visible? and .focused?) | "odio \\\\ dio"' 2>&1)"
bindsym $mod+Print exec notify-send "odio \\ la madonna"

hide_edge_borders smart

# move workspace
bindsym $mod+greater move workspace to output right
bindsym $mod+less move workspace to output right

# bindsym ctrl+Mod1+Delete exec "kill -9 `xdotool getwindowfocus getwindowpid`"

assign [class="^firefox"] $ws1
assign [class="Minecraft"] $ws3
assign [class="SFML test"] $ws3
assign [class="Cantata"] $ws5
assign [class="^Telegram"] $ws5
assign [class="^element"] $ws5
assign [class="^easyeffects"] $ws5
assign [class="^Mumble"] $ws5
assign [class="^nheko"] $ws5
assign [class="^keepassxc"] $ws5
assign [class="^deluge"] $ws6
# assign [class="Chromium"] $ws7

# i love gtk
for_window [class=.*] border pixel 2
for_window [floating] border pixel 3
no_focus [tiling]
for_window [all] tearing_allowed yes

floating_maximum_size 1366 x 758
for_window [all] title_window_icon yes
for_window [window_type="dialog"] floating enable

bindsym XF86MonBrightnessUp exec brightnessctl s 10%+
bindsym XF86MonBrightnessDown exec brightnessctl s 10%-

