import re

# define a gdb command similar to starti but that stops at the entry point instead 
# of the first instruction. Useful for dynamically linked executable to skip the loader
class StartEntry(gdb.Command):
    def __init__(self):
        super().__init__("startentry", gdb.COMMAND_RUNNING)

    def invoke(self, arguments, tty):
        gdb.execute(f"starti")
        out = gdb.execute("info file", to_string=True)
        entry_addr = re.search("Entry point: (0x[0-9a-f]+)", out).group(1)
        gdb.execute(f"break *{entry_addr}")
        gdb.execute(f"continue")
        gdb.execute(f"clear *{entry_addr}")


StartEntry()
