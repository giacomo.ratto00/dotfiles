local mp = require 'mp'

local function on_fps_change(name)
	local video_fps = mp.get_property_native("container-fps")
	local screen_fps = mp.get_property_native("display-fps")
	if (video_fps == nil or screen_fps == nil) then
		return
	end
	local target_fps = math.modf(screen_fps / video_fps)
	-- local video_filter = mp.get_property_native("vf")
	local video_filter = {{
		enabled= true,
		name= "fps",
		params = {
			fps= target_fps .. "*source_fps",
		},
	}}
	mp.set_property_native("vf", video_filter)
end


mp.observe_property("container-fps", "number", on_fps_change)

