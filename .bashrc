# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color|*-256color) color_prompt=yes;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

# export PS1='$([ "$?" == "0" ] && echo -ne "\e[01;32m\$\e[0m" || echo -ne "\e[01;31m\$\e[0m")  $([ "$PWD" != "$HOME" ] && printf "\e[36;01m%s \e[0m" $(basename "$PWD"))'
export PS1='$([ "$PWD" != "$HOME" ] && printf "%s " $(basename "$PWD"))$ '


alias genpass="node -e \"console.log(crypto.randomBytes(18).toString('base64'))\""
man() { nvim -R +"set ft=man" +"Man $1 $2" +"set signcolumn=no" ; }
alias clipcopy='xclip -selection clipboard'
alias clipaste='xclip -selection clipboard -out'
alias getworkspace="i3-msg -t get_workspaces | jq '.[] | select(.focused).name' -r"
if [ -n "$DISPLAY" ]; then
	export PROMPT_COMMAND="pwd > /tmp/pwd\"$(getworkspace)\""
fi
alias revert-xrandr='sleep 8 && xrandr --auto && echo reverted'
alias vim=nvim
. /usr/share/bash-completion/completions/git
alias dotfiles="git --git-dir=$HOME/.dotfiles"
complete -o bashdefault -o default -o nospace -F __git_wrap__git_main dotfiles

pw-stop() {
	systemctl --user stop pipewire-pulse.socket
	systemctl --user stop pipewire-pulse.service
	systemctl --user stop wireplumber
	systemctl --user stop pipewire.socket
	systemctl --user stop pipewire
}

pw-start() {
	systemctl --user start pipewire
	systemctl --user start pipewire.socket
	systemctl --user start wireplumber
	systemctl --user start pipewire-pulse
}

pw-restart() {
	pw-stop
	pw-start
}

x() {
	(/bin/xdg-open $@ &) &>/dev/null 
}
