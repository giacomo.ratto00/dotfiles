# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return;;
esac

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt append_history
setopt hist_ignore_dups

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color|*-256color) color_prompt=yes;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi


[ -f /usr/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh ] && source /usr/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh

autoload -Uz compinit && compinit
autoload -Uz promptinit && promptinit

prompt adam1

zstyle ':completion:*' menu select,interactive
zstyle ':completion:*' file-list all
# zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:default' list-colors "$LS_COLORS"
zstyle ':completion:*' squeeze-slashes true
bindkey '^[[Z' reverse-menu-complete

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias genpass="node -e \"console.log(crypto.randomBytes(18).toString('base64'))\""
man() { nvim -R +"set ft=man" +"Man $1 $2" +"set signcolumn=no" ; }
alias clipcopy='xclip -selection clipboard'
alias clipaste='xclip -selection clipboard -out'
alias getworkspace="i3-msg -t get_workspaces | jq '.[] | select(.focused).name' -r"
if [ -n "$DISPLAY" ] && [ -z "$SSH_TTY" ] && pgrep -x i3 > /dev/null; then
	precmd() { pwd > /tmp/pwd"$(getworkspace)"; }
fi
alias revert-xrandr='sleep 8 && xrandr --auto && echo reverted'
alias vim=nvim
alias dotfiles="git --git-dir=$HOME/.dotfiles"

pw-stop() {
systemctl --user stop pipewire-pulse.socket
systemctl --user stop pipewire-pulse.service
systemctl --user stop wireplumber
systemctl --user stop pipewire.socket
systemctl --user stop pipewire
}

pw-start() {
systemctl --user start pipewire
systemctl --user start pipewire.socket
systemctl --user start wireplumber
systemctl --user start pipewire-pulse
}

pw-restart() {
pw-stop
pw-start
}

x() {
	(/bin/xdg-open $@ &) &>/dev/null 
}

PATH="/home/giacomo/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/giacomo/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/giacomo/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/giacomo/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/giacomo/perl5"; export PERL_MM_OPT;
